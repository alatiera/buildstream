
Install
=======
This section covers how to install BuildStream onto your machine, how to run BuildStream inside a docker image and also how to configure an artifact server.


.. toctree::
   :maxdepth: 2

   install_main
   install_docker
   install_artifacts
